#include "ImGuiWindow.h"

ImGuiWindowFunctor::ImGuiWindowFunctor(std::string name) :
	windowName(name)
{
}

void ImGuiWindowFunctor::operator()()
{
	//Set Flags
	ImGuiWindowFlags flags = SetFlags();

	//Set Pos
	SetPos();

	//Set Size
	SetSize();

	//Begin call : Main body of window starts here.
	if (!ImGui::Begin(windowName.c_str(), NULL, flags))
	{
		//Early out if window is collapsed, optimization.
		ImGui::End();
		return;
	}

	//Main body of window functionality
	MainBody();

	//End of Window
	ImGui::End();
}
