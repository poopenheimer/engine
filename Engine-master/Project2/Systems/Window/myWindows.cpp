#include "myWindows.h"

MyWindows* MyWindows::instance = nullptr;

//The constructor
MyWindows::MyWindows()
{
	//The size passed to CreateWindow is the full size including the windows border and caption 
	//AdjustWindowRect will adjust the provided rect so that the client size of the window is the desired size
	WINDOW_WIDTH = GetSystemMetrics(SM_CXSCREEN);
	WINDOW_HEIGHT = GetSystemMetrics(SM_CYSCREEN);
	RECT WindowDimension = { 0, 0, MyWindows::WINDOW_WIDTH, MyWindows::WINDOW_HEIGHT };

	//AdjustWindowRect
	//1) param - WindowDimension
	//2) param - The style of the window, which must match what is passed in to CreateWindow below
	//			 WS_OVERLAPPEDWINDOW means re-sizeable, our windowed mode is not.
	//3) param - Does this window have a menu? We set it to true
	AdjustWindowRect(&WindowDimension, WS_OVERLAPPEDWINDOW, TRUE);


	//Create the window class for the game
	WNDCLASSEX WindowClass =
	{ sizeof(WNDCLASSEX),				//The size of this structure (passing the size allows Microsoft to update their interfaces and maintain backward compatibility)
	 CS_OWNDC ,							//The style of the window class--this is the base type (one device context for all windows in the process)
	 WndProc,							//The name of the message handling function
	 0L, 0L,							//The amount of extra memory to allocate for this class and window
	 GetModuleHandle(NULL),				//Handle to the instance that has the windows procedure--NULL means use this file.
	 NULL,								//Add an Icon as a resource and add them here -> LoadIcon(NULL, MAKEINTRESOURCE(IDI_APPLICATION))
	 LoadCursor(nullptr, IDC_ARROW),								//Use the default arrow cursor - IDC_ARROW
	 (HBRUSH)COLOR_APPWORKSPACE,		//A handle to the class background brush. COLOR_APPWORKSPACE - for some reason I needed to type_cast the int Macro
	 NULL,								//Pointer to a null-terminated character string that specifies the resource name of the class menu
	 myWindowClassName.c_str(),			//The window class name
	 NULL };							//The small icon (NULL just uses the default)

	//WindowClass.style = WS_SYSMENU;
	

	//Register the window class
	RegisterClassEx(&WindowClass);

	//Store the handle to the instance
	m_hInstance = WindowClass.hInstance;

	//Create the game's window
	m_hWnd = CreateWindow(
		myWindowClassName.c_str(),								//The window class name
		myWindowClassName.c_str(),								//The name for the title bar
		WS_OVERLAPPEDWINDOW /* ^ WS_THICKFRAME */,				//The style of the window (WS_BORDER, WS_MINIMIZEBOX, WS_MAXIMIZE, etc.)
		CW_USEDEFAULT,											//The x and y position of the window -screen coords for base window
		CW_USEDEFAULT,											//The x and y position of the window -relative coords for child windows
		WindowDimension.right - WindowDimension.left,			//Width of the window, including borders
		WindowDimension.bottom - WindowDimension.top,			//Height of the window, including borders and caption
		GetDesktopWindow(),										//The parent window
		NULL,													//The menu for the window - it can be NULL if the class menu is to be used
		m_hInstance,											//The handle to the instance of the window class
		NULL);

	//Do we want to be able to drag and drop files? -> shellapi.h
	DragAcceptFiles(m_hWnd, true);

}
//The destructor
MyWindows::~MyWindows()
{
	//Unregister the window class
	UnregisterClass(myWindowClassName.c_str(), m_hInstance);
}


/******************************************************************************/
/*!
  \brief
	Activates and make the windows visiable  once the windows class has
	been registered.
*/
/******************************************************************************/
void MyWindows::ActivateWindow()
{
	//Show the window (could also be SW_SHOWMINIMIZED, SW_SHOWMAXIMIZED, etc.)
	ShowWindow(m_hWnd, SW_SHOWMAXIMIZED);
	//After showing the window, we should inform the OS to update the client area of our window by
	//generating a WM_PAINT message.This is done by calling the “UpdateWindow” function
	UpdateWindow(m_hWnd);

	windowActive = true;
}

/******************************************************************************/
/*!
  \brief
	Updates the window every frame.
*/
/******************************************************************************/
void MyWindows::Update()
{
	MSG msg;

	UpdateWindow(m_hWnd);
	//PeekMessage
	//Look for any pending windows messages, remove them, then handle them
	//The second parameter is the window handle--NULL just means get any message from the current thread
	//The third and forth parameters are the start and end message types to process
	//The last parameter determines whether or not the message is removed
	//It is important to get all windows messages available not just one
	while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
	{
		TranslateMessage(&msg);	//Makes sure WM_CHAR and similar messages are generated
		DispatchMessage(&msg);	//Calls the message procedure (see below) with this message

								//If we get a quit message, broadcast it to all systems
		if (msg.message == WM_QUIT)
		{
			//Signal to game to quit.
			windowActive = false;
		}
	}

	// Ensure that Window's width and height are updates.
	WINDOW_WIDTH = GetClientRect().right - GetClientRect().left;
	WINDOW_HEIGHT = GetClientRect().bottom - GetClientRect().top;

}

/******************************************************************************/
/*!
  \brief
	Gets the window's current RECT that consists of
	1. Top
	2. Bottom
	3. Left
	4. Right

	To get window's width, Right - Left.
	To get window's height, Bottom - Top.
*/
/******************************************************************************/
RECT MyWindows::GetClientRect()
{
	RECT WindowDimension;
	GetWindowRect(m_hWnd, &WindowDimension);

	return WindowDimension;
}

