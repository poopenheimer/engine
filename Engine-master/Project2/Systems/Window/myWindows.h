#pragma once

#include <windows.h>
#include <string>
#include <iostream>
#include "../../Input/InputManager.h"

class MyWindows 
{
private: 
	static MyWindows* instance;
public:

	static MyWindows* GetInstance()
	{
		if (!instance)
			instance = new MyWindows();
		return instance;
	}

	//The constructor
	MyWindows();
	//The destructor
	~MyWindows();

	bool windowActive = false;

	//Activate the game window so it is actually visible
	void ActivateWindow();
	//Update this system every frame
	void Update();

	//Handle to the game window
	HWND		m_hWnd;
	//Handle to the instance, NOTE: an application can have multiple HWND windows but only one instance
	HINSTANCE	m_hInstance;


	std::string myWindowClassName = "MYWINDOWS";
	int WINDOW_WIDTH = 1980;
	int WINDOW_HEIGHT = 1280;

	RECT GetClientRect();
};
//LRESULT WINAPI InputHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);