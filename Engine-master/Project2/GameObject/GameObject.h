#pragma once
#include <vector>
#include "Component.h"
#include "Script.h"
#include "../Math/Math.h"

class GameObject
{
public:
	int UID;
	std::vector<Component*> components;
	std::vector<Script*> scripts;

	float rotation;
	Vec3 position;
	Vec2 scale;

	GameObject();
	virtual ~GameObject() ;
};