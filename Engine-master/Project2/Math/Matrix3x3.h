#pragma once
#include <string>
#include <sstream>
#include <math.h>

class Matrix3x3
{
private:

public:
	float m00, m01, m02;
	float m10, m11, m12;
	float m20, m21, m22;

	Matrix3x3();
	Matrix3x3(	float M00, float M01, float M02, 
				float M10, float M11, float M12,
				float M20, float M21, float M22	);
	Matrix3x3(const Matrix3x3& other);

	~Matrix3x3();

	Matrix3x3& operator=(const Matrix3x3& other);
	Matrix3x3 operator+(const Matrix3x3& other) const;
	void operator+=(const Matrix3x3& other);
	Matrix3x3 operator*(const float scalar) const;
	void operator*=(const float scalar);
	Matrix3x3 operator*(const Matrix3x3& other) const;
	void operator*=(const Matrix3x3& other);

	//Changes this matrix into an identity matrix.
	void SetIdentity();
	//Changes this matrix by transposing. Reflects the values about the diagonal.
	void Transpose();
	//Returns the determinant of this matrix.
	float Determinant() const;
	//Set this matrix to the inverse of the input matrix.
	void GetInverseOf(const Matrix3x3& other);
	//Get inverse of this matrix. If inverse is not possible, returns the same matrix.
	Matrix3x3 GetInverse() const;

	//Returns an identity matrix.
	static Matrix3x3 IdentityMatrix();
	//Returns a translation transformation matrix with given x and y values representing translation amount.
	static Matrix3x3 TranslationMatrix(float x, float y);
	//Returns a scale transformation matrix with given x and y values representing scale amount.
	static Matrix3x3 ScaleMatrix(float x, float y);
	//Returns a rotation transformation matrix formed using a given angle in degrees.
	static Matrix3x3 RotationMatrixDeg(float angleInDeg);
	//Returns a rotation transformation matrix formed using a given angle in radians.
	static Matrix3x3 RotationMatrixRad(float angleInRad);

	std::string GetFormattedString() const;
};

