#pragma once
#include <glew.h>
#include <string>
#include <../glm/glm.hpp>

class MyGLSL
{
	static MyGLSL * instance;
	void CheckCompileError(GLuint id, std::string type);
	void CompileShaders(const GLchar* vertexPath, const GLchar* fragmentPath);

	bool vertexShaderIsCompiled = false;
	bool fragmentShaderIsCompiled = false;
	GLuint vShader;
	GLuint fShader;
public:
	static MyGLSL * GetInstance()
	{
		if(!instance)
			instance = new MyGLSL();
		return instance;
	}
	GLuint m_ShaderID;


	void f_LoadShaderString(const GLchar* vertexPath, const GLchar* fragmentPath);
	GLuint GetVShader();
	GLuint GetFShader();
	GLuint GetShaderID();
	~MyGLSL();
	void Start();
	void Clean();
	void End();
};