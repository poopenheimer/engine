#include "MyGLSL.h"
#include "iostream"
#include <fstream>
#include <sstream>

MyGLSL * MyGLSL::instance = nullptr;

void MyGLSL::CompileShaders(const GLchar* vertexPath, const GLchar* fragmentPath)
{
	// Create Vertex Shader
	 vShader = glCreateShader(GL_VERTEX_SHADER); // create a shader object
	glShaderSource(vShader, 1, &vertexPath, NULL); // replacing the shader source code
	glCompileShader(vShader);
	CheckCompileError(vShader, "VERTEX");
	vertexShaderIsCompiled = true;

	// Create Fragment Shader
	 fShader = glCreateShader(GL_FRAGMENT_SHADER); // create a shader object
	glShaderSource(fShader, 1, &fragmentPath, NULL); // replacing the shader source code
	glCompileShader(fShader);
	CheckCompileError(fShader, "FRAGMENT");
	fragmentShaderIsCompiled = true;

	// Create program shader. (later attach shaders, and link it)
	m_ShaderID = glCreateProgram();

	// attach shader object to the main shader program (must be different type)
	glAttachShader(m_ShaderID, vShader);
	glAttachShader(m_ShaderID, fShader);
	glLinkProgram(m_ShaderID); //link all attached shaders in to the main shader

	CheckCompileError(m_ShaderID, "PROGRAM");

	glDeleteShader(vShader);
	glDeleteShader(fShader);

}

void MyGLSL::f_LoadShaderString(const GLchar* vertexPath, const GLchar* fragmentPath)
{
	std::string vCode, fCode;

	try
	{
		std::ifstream vFile(vertexPath);
		std::ifstream fFile(fragmentPath);
		std::stringstream vStream, fStream;
		// create std::string that contains the data in the File
		vStream << vFile.rdbuf();
		fStream << fFile.rdbuf();
		vFile.close();
		fFile.close();
		vCode = vStream.str();
		fCode = fStream.str();
	}
	catch (std::exception e)
	{
		std::cout << "ERROR: Failed to read shader file" << std::endl;
	}


	CompileShaders(vCode.c_str(), fCode.c_str());

}

void MyGLSL::CheckCompileError(GLuint id, std::string type) // check for compile error
{
	GLint success;
	GLchar infoLog[512];

	if (type != "PROGRAM")
	{
		glGetShaderiv(id, GL_COMPILE_STATUS, &success); //verifies if the compilation is a success
		if (!success)
		{
			// get the error log and print the error message
			glGetShaderInfoLog(id, 512, NULL, infoLog);
			std::cout << "ERROR::" << type << " SHADER: FAIL TO COMPILE" << infoLog << std::endl;
		}
	}
	else
	{
		glGetShaderiv(id, GL_COMPILE_STATUS, &success); //verifies if the compilation is a success
		if (!success)
		{
			// get the error log and print the error message
			glGetShaderInfoLog(id, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER " << type << " FAIL TO LINK" << infoLog << std::endl;
		}
	}
}

void MyGLSL::Start()
{
	glUseProgram(m_ShaderID);
}

void MyGLSL::Clean()
{
	if (vertexShaderIsCompiled)
	{
		glDetachShader(m_ShaderID, vShader);
		glDeleteShader(vShader);
	}

	if (fragmentShaderIsCompiled)
	{
		glDetachShader(m_ShaderID, fShader);
		glDeleteShader(fShader);
	}

	if (m_ShaderID)
		glDeleteShader(m_ShaderID);

	std::cout << "Successfully Cleared shader program" << std::endl;
}

GLuint MyGLSL::GetFShader()
{
	return fShader;
}

GLuint MyGLSL::GetVShader()
{
	return vShader;
}

GLuint MyGLSL::GetShaderID()
{
	return m_ShaderID;
}

void MyGLSL::End()
{
	glUseProgram(0);
}