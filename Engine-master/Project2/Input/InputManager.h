#pragma once
#include <windows.h>
#include <iostream>

// Keys are mapped according to windows virtual key codes
enum KB_INPUT
{
	SHIFT = 0X10,
	CTRL, ALT,

	SPACE = 0x20,

	LEFT = 0x25,
	UP, RIGHT, DOWN,

	KEY0 = 0x30,
	KEY1, KEY2, KEY3,
	KEY4, KEY5, KEY6,
	KEY7, KEY8, KEY9,

	ESC = 0x1B,

	A = 0x41,
	B, C, D, E, F,
	G, H, I, J, K,
	L, M, N, O, P,
	Q, R, S, T, U,
	V, W, X, Y, Z,

	KEYDELETE = 0x2E,
};

enum M_INPUT
{
	LMOUSEB = 0x01,
	RMOUSEB,
	MIDMOUSEB = 0x04,
	LMOUSE_DOUBLE_CLICK,
	RMOUSE_DOUBLE_CLICK,
};

enum M_SCROLLER
{
	IN_NOSCROLL,
	IN_SCROLLUP,
	IN_SCROLLDOWN
};

class InputManager
{
private:
	static InputManager* instance;
	
	InputManager() {};
	~InputManager() {};

	bool m_PrevStates[256];
	bool m_CurrStates[256];

public:
	static InputManager* GetInstance()
	{
		if (!instance)
			instance = new InputManager();
		return instance;
	}

	//Initialize, read and reset the buttons
	void	Initialize();
	void	ReadInput(UINT key);		//Read what is pressed
	void	ResetInput(UINT key);		//Reset the state.

	bool	KeyPressed(UINT key);		//Press and hold
	bool	KeyDown(UINT key);			//Press and release
};

LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


