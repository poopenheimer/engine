#pragma once

#include <glew.h>

namespace GAMEENGINE
{
	class CoreEngine
	{
		static CoreEngine* instance;
	public:
		CoreEngine() {};
		~CoreEngine() {};

		static CoreEngine* GetInstance()
		{
			if (!instance)
				instance = new CoreEngine();
			return instance;
		}

		void Initialize();
		void Reinitialize();
		void MainGameLoop();

		void CleanUp();
	};
}
