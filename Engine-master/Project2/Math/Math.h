#pragma once
#include <../glm/glm.hpp>
#include <../glm/gtc/matrix_transform.hpp>

typedef glm::vec2 Vector2;
typedef glm::vec2 Vec2;
typedef glm::vec3 Vec3;