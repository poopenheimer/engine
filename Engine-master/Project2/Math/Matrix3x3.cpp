#include "Matrix3x3.h"

Matrix3x3::Matrix3x3() :
	m00{0.0f}, m01{0.0f}, m02{0.0f},
	m10{0.0f}, m11{0.0f}, m12{0.0f},
	m20{0.0f}, m21{0.0f}, m22{0.0f}
{}

Matrix3x3::Matrix3x3(float M00, float M01, float M02, float M10, float M11, float M12, float M20, float M21, float M22)
{
	m00 = M00;
	m01 = M01;
	m02 = M02;
	m10 = M10;
	m11 = M11;
	m12 = M12;
	m20 = M20;
	m21 = M21;
	m22 = M22;
}

Matrix3x3::Matrix3x3(const Matrix3x3& other) :
	Matrix3x3()
{
	*this = other;
}

Matrix3x3& Matrix3x3::operator=(const Matrix3x3& other)
{
	m00 = other.m00;
	m01 = other.m01;
	m02 = other.m02;
	m10 = other.m10;
	m11 = other.m11;
	m12 = other.m12;
	m20 = other.m20;
	m21 = other.m21;
	m22 = other.m22;

	return *this;
}


Matrix3x3::~Matrix3x3()
{
}

Matrix3x3 Matrix3x3::operator+(const Matrix3x3& other) const
{
	return Matrix3x3(	m00 + other.m00, m01 + other.m01, m02 + other.m02,
						m10 + other.m10, m11 + other.m11, m12 + other.m12, 
						m20 + other.m20, m21 + other.m21, m22 + other.m22);
}

void Matrix3x3::operator+=(const Matrix3x3& other)
{
	m00 += other.m00;
	m01 += other.m01;
	m02 += other.m02;
	m10 += other.m10;
	m11 += other.m11;
	m12 += other.m12;
	m20 += other.m20;
	m21 += other.m21;
	m22 += other.m22;
}

Matrix3x3 Matrix3x3::operator*(const float scalar) const
{
	return Matrix3x3(	m00 * scalar, m01 * scalar, m02 * scalar,
						m10 * scalar, m11 * scalar, m12 * scalar,
						m20 * scalar, m21 * scalar, m22 * scalar);
}

void Matrix3x3::operator*=(const float scalar)
{
	m00 *= scalar;
	m01 *= scalar;
	m02 *= scalar;
	m10 *= scalar;
	m11 *= scalar;
	m12 *= scalar;
	m20 *= scalar;
	m21 *= scalar;
	m22 *= scalar;
}

Matrix3x3 Matrix3x3::operator*(const Matrix3x3& other) const
{
	return Matrix3x3(	m00 * other.m00 + m01 * other.m10 + m02 * other.m20,
						m00 * other.m01 + m01 * other.m11 + m02 * other.m21,
						m00 * other.m02 + m01 * other.m12 + m02 * other.m22,

						m10 * other.m00 + m11 * other.m10 + m12 * other.m20,
						m10 * other.m01 + m11 * other.m11 + m12 * other.m21,
						m10 * other.m02 + m11 * other.m12 + m12 * other.m22,

						m20 * other.m00 + m21 * other.m10 + m22 * other.m20,
						m20 * other.m01 + m21 * other.m11 + m22 * other.m21,
						m20 * other.m02 + m21 * other.m12 + m22 * other.m22);
}

void Matrix3x3::operator*=(const Matrix3x3& other)
{
	float tmp1, tmp2, tmp3;

	tmp1 = m00 * other.m00 + m01 * other.m10 + m02 * other.m20;
	tmp2 = m00 * other.m01 + m01 * other.m11 + m02 * other.m21;
	tmp3 = m00 * other.m02 + m01 * other.m12 + m02 * other.m22;
	m00 = tmp1;
	m01 = tmp2;
	m02 = tmp3;

	tmp1 = m10 * other.m00 + m11 * other.m10 + m12 * other.m20;
	tmp2 = m10 * other.m01 + m11 * other.m11 + m12 * other.m21;
	tmp3 = m10 * other.m02 + m11 * other.m12 + m12 * other.m22;
	m10 = tmp1;
	m11 = tmp2;
	m12 = tmp3;

	tmp1 = m20 * other.m00 + m21 * other.m10 + m22 * other.m20;
	tmp2 = m20 * other.m01 + m21 * other.m11 + m22 * other.m21;
	tmp3 = m20 * other.m02 + m21 * other.m12 + m22 * other.m22;
	m20 = tmp1;
	m21 = tmp2;
	m22 = tmp3;
}

void Matrix3x3::SetIdentity()
{
	m00 = m11 = m22 = 1.0f;
	m01 = m02 = m10 = m12 = m20 = m21 = 0.0f;
}

void Matrix3x3::Transpose()
{
	Matrix3x3 transposed;
	transposed.m00 = m00;
	transposed.m11 = m11;
	transposed.m22 = m22;

	transposed.m01 = m10;
	transposed.m10 = m01;
	transposed.m02 = m20;
	transposed.m20 = m02;
	transposed.m12 = m21;
	transposed.m21 = m12;

	*this = transposed;
}

float Matrix3x3::Determinant() const
{
	return	m00 * (m11 * m22 - m12 * m21) -
			m01 * (m10 * m22 - m12 * m20) +
			m02 * (m10 * m21 - m11 * m20);
}

void Matrix3x3::GetInverseOf(const Matrix3x3& other)
{
	float det = other.Determinant();

	if (det == 0) return;

	float oneOverDet = 1.0f / det;

	Matrix3x3 t_Other = other;
	t_Other.Transpose();
	Matrix3x3 adj_Other(	(t_Other.m11 * t_Other.m22 - t_Other.m12 * t_Other.m21),
						-	(t_Other.m10 * t_Other.m22 - t_Other.m12 * t_Other.m20),
							(t_Other.m10 * t_Other.m21 - t_Other.m11 * t_Other.m20),
						-	(t_Other.m01 * t_Other.m22 - t_Other.m02 * t_Other.m21),
							(t_Other.m00 * t_Other.m22 - t_Other.m02 * t_Other.m20),
						-	(t_Other.m00 * t_Other.m21 - t_Other.m01 * t_Other.m20),
							(t_Other.m01 * t_Other.m12 - t_Other.m02 * t_Other.m11),
						-	(t_Other.m00 * t_Other.m12 - t_Other.m02 * t_Other.m10),
							(t_Other.m00 * t_Other.m11 - t_Other.m01 * t_Other.m10));

	*this = adj_Other * oneOverDet;
}

Matrix3x3 Matrix3x3::GetInverse() const
{
	Matrix3x3 inverse;
	inverse.GetInverseOf(*this);
	return inverse;
}

Matrix3x3 Matrix3x3::IdentityMatrix()
{
	Matrix3x3 identity;
	identity.SetIdentity();
	return identity;
}

Matrix3x3 Matrix3x3::TranslationMatrix(float x, float y)
{
	Matrix3x3 translation;
	translation.SetIdentity();
	translation.m02 = x;
	translation.m12 = y;
	return translation;
}

Matrix3x3 Matrix3x3::ScaleMatrix(float x, float y)
{
	Matrix3x3 scale;
	scale.SetIdentity();
	scale.m00 = x;
	scale.m11 = y;
	return scale;
}

Matrix3x3 Matrix3x3::RotationMatrixDeg(float angleInDeg)
{
	float angleInRad = angleInDeg * static_cast<float>(acos(-1) / 180.0f);
	return RotationMatrixRad(angleInRad);
}

Matrix3x3 Matrix3x3::RotationMatrixRad(float angleInRad)
{
	Matrix3x3 rot;
	rot.SetIdentity();
	rot.m00 = rot.m11 = cosf(angleInRad);
	rot.m01 = -sinf(angleInRad);
	rot.m10 = -rot.m01;

	return rot;
}

std::string Matrix3x3::GetFormattedString() const
{
	std::ostringstream mString;
	mString << "[" << m00 << "," << m01 << "," << m02 << "]" << std::endl;
	mString << "[" << m10 << "," << m11 << "," << m12 << "]" << std::endl;
	mString << "[" << m20 << "," << m21 << "," << m22 << "]" << std::endl;
	return mString.str();
}
