#pragma once

#include <string>

#include "External/imgui.h"

/**
Functor object encapsulating the concept of a Window in ImGui.
Requires the following function definitions in derived types:
	-SetFlags()
	-SetPos()
	-SetSize()
	-MainBody()
*/
class ImGuiWindowFunctor
{
	virtual ImGuiWindowFlags SetFlags() = 0;
	virtual void SetPos() = 0;
	virtual void SetSize() = 0;
	virtual void MainBody() = 0;

public:
	
	ImGuiWindowFunctor(std::string name);

	void operator()();

	std::string windowName;
};













