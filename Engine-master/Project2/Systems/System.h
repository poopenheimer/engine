#pragma once

#include "../Systems/Window/myWindows.h"
#include "../../Input/InputManager.h"
#include "../Systems/Graphics/GraphicsSystem.h"
#include "../Systems/ImGui/MyImGui.h"


class System
{
private:
	static System* instance;

	//Destructor for System
	~System() {}

public:
	static System* GetInstance()
	{
		if (!instance)
			instance = new System();
		return instance;
	}
	// Initializes the system
	void System_Initialize();

	// Resets all initialized variables
	void System_Reset();

	// Exit the system
	void AllSystem_Exit();

	void Exit();
};