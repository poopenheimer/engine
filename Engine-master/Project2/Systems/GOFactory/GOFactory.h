#pragma once
#include <vector>
#include "../../GameObject/GameObject.h"

class GOFactory
{
public:
	std::vector<GameObject*> AllGameObjects;

	void Initialize();
	void Update();

	GameObject * CreateEmptyGO();
	void DestroyGO(GameObject * go);
	void DestroyAllGO();



};