#pragma once

#include <string>

#include "ImGuiWindow.h"


class InspectorWindow : public ImGuiWindowFunctor
{


public:

	InspectorWindow(std::string name);

	ImGuiWindowFlags SetFlags();
	void SetPos();
	void SetSize();
	void MainBody();

};



