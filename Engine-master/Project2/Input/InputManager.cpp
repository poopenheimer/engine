#pragma once
#include "inputManager.h"
#include "../Systems/System.h"
#include "../Systems/ImGui/MyImGui.h"

InputManager* InputManager::instance = nullptr;

//The message handling procedure for the game
LRESULT CALLBACK WndProc(HWND hWnd,	 //The window the message is for (ours in this case)
						 UINT msg,	   //The message type(Key code based on windows)
					     WPARAM wParam, //The primary data for the message
						 LPARAM lParam) //The secondary data for the message (if any)
{
	
	// Pass msg to imgui as well
	if (ImGuiSystem::GetInstance()->initEditor)
		ImGuiSystem::GetInstance()->ImProcessInput(hWnd, msg, wParam, lParam);

	//See what type of message it is
	switch (msg)
	{
	case WM_CHAR: //A character key was pressed
	{
		std::cout << wParam << " was pressed." << std::endl;
		break;
	}
	case WM_ENTERSIZEMOVE:
	{
		std::cout << "Windows Resizing start/ moving" << std::endl;
		break;
	}

	case WM_EXITSIZEMOVE:
	{
		std::cout << "Windows Resizing/ moving End" << std::endl;
		break;
	}
	case WM_LBUTTONDBLCLK:
	{

		break;
	}
	case WM_LBUTTONDOWN:
	{

		std::cout << wParam << std::endl;
		std::cout << "Larry pRess Lmouse" << std::endl;
		break;
	}
	case WM_RBUTTONDOWN:
	{
		std::cout << "Larry pRess RMouse" << std::endl;
		std::cout << wParam << std::endl;
		break;
	}
	case WM_LBUTTONUP:
	{
		std::cout << "Larry pRess Lmouse UP" << std::endl;
		break;
		break;
	}
	case WM_RBUTTONUP:
	{
		//MouseButton m(MouseButton::RightMouse, false, Vec2(WINDOWSSYSTEM->MousePosition.x, WINDOWSSYSTEM->MousePosition.y));
		//CORE->BroadcastMessage(&m);
		break;
	}
	case WM_MOUSEMOVE:
	{

		break;
	}
	case WM_KEYDOWN:
		//A key was pressed
		//TODO: Handle any key logic you might need for game controls
		//Use virtual key codes (VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN, VK_SPACE, etc.)
		//to detect specific keys (the wParam is the key pressed) and then
		//broadcast whatever message you need
		switch (wParam)
		{
		case VK_LEFT:

			// Process the LEFT ARROW key. 
			InputManager::GetInstance()->ReadInput(LEFT);
			break;

		case VK_RIGHT:

			// Process the RIGHT ARROW key. 

			break;

		case VK_UP:

			// Process the UP ARROW key.
			GraphicsSystem::GetInstance()->bgColor += 0.01f;


			break;

		case VK_DOWN:

			// Process the DOWN ARROW key. 
			GraphicsSystem::GetInstance()->bgColor -= 0.01f;
			break;

		case VK_HOME:

			// Process the HOME key. 

			break;

		case VK_END:

			// Process the END key. 

			break;

		case VK_INSERT:

			// Process the INS key. 

			break;

		case VK_DELETE:

			// Process the DEL key. 

			break;

		case VK_F2:

			// Process the F2 key. 

			break;


			// Process other non-character keystrokes. 

		default:
			break;
		}




		break;
	case WM_KEYUP: //A key was released
				   //TODO: Handle any key logic you might need for game controls
		break;

	case WM_SIZE:
	{

		break;
	}
	case WM_PAINT:
	{
		break;
	}
	case WM_DESTROY: //A destroy message--time to kill the game
					 //Make sure we shut everything down properly by telling Windows
					 //to post a WM_QUIT message (the parameter is the exit code).
		PostQuitMessage(0);
		return 0;
	case WM_DROPFILES:
	{

		break;

	}
		

	case WM_SYSKEYDOWN:
	{
		//Eat the WM_SYSKEYDOWN message to prevent freezing the game when
		//the alt key is pressed
		switch (wParam)
		{
		case VK_LMENU:
		case VK_RMENU:
			return 0;//Stop Alt key from stopping the winProc
		case VK_F4:
			//Check for Alt F4
			DWORD dwAltKeyMask = (1 << 29);
			if ((lParam & dwAltKeyMask) != 0)
				PostQuitMessage(0);
			return 0;
		//case KBINPUT::A:
		//	std::cout << "asdas" << std::endl;
		//	return 0;
		}
		return 0;//
	}
	}
	//TODO: Handle mouse messages and other windows messages

	//We didn't completely handle the message, so pass it on for Windows to handle.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void InputManager::Initialize()
{
	//f_SetWindowPos(0, 0);
	for (int i = 0; i < 256; ++i)
		m_PrevStates[i] = m_CurrStates[i] = false;
}

void InputManager::ReadInput(UINT key)
{
	m_CurrStates[key] = true;
}

void InputManager::ResetInput(UINT key)
{
	m_CurrStates[key] = false;
}

bool InputManager::KeyPressed(UINT key)
{
	// If current key is press and previous key is not pressed
	if (m_CurrStates[key] && !m_PrevStates[key])
		return true;

	return false;
}

bool InputManager::KeyDown(UINT key)
{
	if (m_CurrStates[key])
		return true;

	return false;
}


