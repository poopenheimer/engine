#pragma once

#include <vector>
#include <memory>

#include "glew.h"
#include "../ImGui/External/imgui.h"
#include "../Graphics/MyGLSL.h"
#include "../Window/myWindows.h"
#include "ImGuiWindow.h"

//All window includes
#include "Hierarchy.h"
#include "Inspector.h"

class ImGuiSystem
{
	static ImGuiSystem * instance;

public:
	
	std::vector<std::unique_ptr<ImGuiWindowFunctor>> allWindows;

	static ImGuiSystem * GetInstance()
	{
		if (!instance)
			instance = new ImGuiSystem();
		return instance;
	}

	~ImGuiSystem();

	void AddAllWindows();

	void Initialize();
	void Update();
	void ImNewFrame();

	void ImProcessInput(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	bool initEditor = false;
};

