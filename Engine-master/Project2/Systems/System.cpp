
#include "System.h"

System* System::instance = nullptr;

MyWindows* WINDOWS = MyWindows::GetInstance();
InputManager* INPUTS = InputManager::GetInstance();
GraphicsSystem* GRAPHICS = GraphicsSystem::GetInstance();
ImGuiSystem* IMGUI = ImGuiSystem::GetInstance();


/*
* This function initializes all system
*/
void System::System_Initialize()
{
	// Initialize the system window
	WINDOWS ->ActivateWindow();
	INPUTS->Initialize();
	GRAPHICS->Initialize();
	IMGUI->GetInstance()->Initialize();


	//INPUTS = InputManager::GetInstance();

}

void System::System_Reset()
{
	//GRAPHICS->Exit();
	//AESysReset();

	////Reset core systems
	//UI->Reset();
	//CAMERA->Reset();
	//RENDERER->Reset();
	//OBJECTS->Reset();
	//PHYSICS->Reset();
	//INPUTS->ResetPlayerInputs();
	//SOUNDS->Reset();
}

void System::AllSystem_Exit()
{
	GRAPHICS->Exit();
}
