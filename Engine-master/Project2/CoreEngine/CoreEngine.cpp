#include "CoreEngine.h"

#include "../Systems/System.h"				//All core systems
#include "../GSM/GameStateManager.h"	//Game State Manager 


namespace GAMEENGINE
{
	CoreEngine* CoreEngine::instance = nullptr;


	void CoreEngine::Initialize()
	{
		//All System Initialize
		System::GetInstance()->System_Initialize();
		//GSM Initialize
		GameStateManager::GetInstance()->GSM_Initialize(GS_TESTLVL);
	}

	void CoreEngine::MainGameLoop()
	{
		while (GameStateManager::GetInstance()->GetCurrentGS() != GS_QUIT)
		{
			// Reset the system modules
			System::GetInstance()->System_Reset();

			// If not restarting, load the gamestate
			GameStateManager::GetInstance()->CheckIfLoadGameState();

			// Initialize the gamestate
			GameStateManager::GetInstance()->foInitGS();

			while (GameStateManager::GetInstance()->GetCurrentGS() == GameStateManager::GetInstance()->GetNextGS())
			{
				//AESysFrameStart();

				//AEInputUpdate();
				MyWindows::GetInstance()->Update();

				GameStateManager::GetInstance()->foUpdateGS();
				GameStateManager::GetInstance()->foDrawGS();

				//AESysFrameEnd();

				// check if forcing the application to quit
				GameStateManager::GetInstance()->CheckIfQuitState();

				// Update All Systems
				MyWindows::GetInstance()->Update();
				GraphicsSystem::GetInstance()->Update();
				ImGuiSystem::GetInstance()->Update();

				//Always swap buffers at the end of all the update loops
				GraphicsSystem::GetInstance()->SwapBuffers();

				//g_dt = (f32)AEFrameRateControllerGetFrameTime();
				//g_appTime += g_dt;
			}

			// Free all objects created
			GameStateManager::GetInstance()->foFreeGS();

			if (GameStateManager::GetInstance()->GetNextGS() != GS_RESTART)
				GameStateManager::GetInstance()->foUnloadGS();

			// PrevGS = CurrGS , CurrGS = NextGS
			GameStateManager::GetInstance()->SetPreviousAndCurrentGS();
		}
	}

	void CoreEngine::CleanUp()
	{
		// Free the system
		System::GetInstance()->AllSystem_Exit();
		//System::GetInstance()->Exit();
	}


}

