#pragma once

#include <functional>
#include "GameStateList.h"
#include "../TestLevel.h"
typedef std::function<void()> GS_Function;

class GameStateManager
{
private:
	static GameStateManager* instance;
	int _currentGS, _previousGS, _nextGS;

	//Constructor for GameStateManager
	GameStateManager() {}

	//Destructor for GameStateManager
	~GameStateManager() {}
public:
	//Gamestate function object that holds 
	GS_Function foLoadGS, foInitGS, foUpdateGS, foDrawGS, foFreeGS, foUnloadGS;
	static GameStateManager* GetInstance()
	{
		if (!instance)
			instance = new GameStateManager();
		return instance;
	}

	int GetCurrentGS(); /* Get Current GameState from _currentGS */
	int GetPreviousGS(); /* Get Previous GameState from _previousGS */
	int GetNextGS(); /* Get Next GameState from _nextGS */

	/* Check if state is restart, if not, load the game state */
	void CheckIfLoadGameState();

	/* Check if state is quit, if it is, quite game */
	void CheckIfQuitState();

	/* Change the value of _nextGS to proceed to next game state */
	void ChangeNextGameState(unsigned int levelInput);

	/* This function Initializes all game state variables */
	void GSM_Initialize(int startingState);

	/* This function updates all game state variables */
	void GSM_Update();

	/* Sets current state to previous state and next state to current state */
	void SetPreviousAndCurrentGS();

	void Exit();
};