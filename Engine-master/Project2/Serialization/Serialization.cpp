#include "Serialization.h"
#pragma once

Serializer * Serializer::instance = nullptr;
void Serializer::SaveData()
{
	tinyxml2::XMLDocument m_Doc;	// Create Doc
	tinyxml2::XMLElement * m_ele = m_Doc.NewElement("GO");		//Create element

	m_ele->SetAttribute("X", vec.x);
	m_ele->SetAttribute("Y", vec.y);
	m_ele->SetAttribute("string", str.c_str());
	m_ele->SetAttribute("Float", f1);

	m_Doc.InsertFirstChild(m_ele);
	m_Doc.SaveFile("GAMEOBJ.xml");
}

void Serializer::LoadData()
{
	tinyxml2::XMLDocument m_Doc;
	m_Doc.LoadFile("GAMEOBJ.xml");

	tinyxml2::XMLElement * m_ele = m_Doc.FirstChildElement();		//Get Root Node

	m_ele->QueryFloatAttribute("X", &vec.x);
	m_ele->QueryFloatAttribute("Y", &vec.y);

	const char* charr = nullptr;
	m_ele->QueryStringAttribute("string", &charr);
	str = charr;

	m_ele->QueryFloatAttribute("Float", &f1);

}
