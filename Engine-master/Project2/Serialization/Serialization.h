#pragma once

#include "TinyXML/tinyxml2.h"
#include <string>
#include "../Math/Math.h"

class Serializer
{
	static Serializer *  instance;
public:

	static Serializer* GetInstance()
	{
		if (!instance)
			instance = new Serializer;

		return instance;
	}

	Serializer() {};

	Vec2			vec{ 0.0f,0.0f };
	std::string		str{ "AAA" };
	float			f1{ 1.6f };

	// Create file


	// Save Data to file
	void SaveData();
	void LoadData();
	
	// Save file to windows directory

	// Load file from windows directory

	// Read file and display  accordingly

	//Create an overload implementation

};