#include "GameStateManager.h"

GameStateManager* GameStateManager::instance = nullptr;
TestLevel testLevel;

/*
* This function Initializes all game state variables
* It should be called after system initialization
*/
void GameStateManager::GSM_Initialize(int startingState)
{
	_currentGS = _previousGS = _nextGS = startingState;
}

/*
* This function updates all game state variables
* It should be called after system initialization
*/
void GameStateManager::GSM_Update()
{
	if ((_currentGS == GS_RESTART) || (_currentGS == GS_QUIT))
		return;
	switch (_currentGS)
	{
	case GS_TESTLVL:
		foLoadGS = std::bind(&TestLevel::TestLevelLoadGS, &testLevel);
		foInitGS = std::bind(&TestLevel::TestLevelInitGS, &testLevel);
		foUpdateGS = std::bind(&TestLevel::TestLevelUpdateGS, &testLevel);
		foDrawGS = std::bind(&TestLevel::TestLevelDrawGS, &testLevel);
		foFreeGS = std::bind(&TestLevel::TestLevelFreeGS, &testLevel);
		foUnloadGS = std::bind(&TestLevel::TestLevelUnloadGS, &testLevel);
		break;
	default:
		break;
	}
}

/* Gets the value of _currentGS */
int GameStateManager::GetCurrentGS()
{
	return _currentGS;
}

/* Gets the value of _previousGS */
int GameStateManager::GetPreviousGS()
{
	return _previousGS;
}

/* Gets the value of _nextGS */
int GameStateManager::GetNextGS()
{
	return _nextGS;
}

/* Check if current gamestate is not restart, load the game state */
void GameStateManager::CheckIfLoadGameState()
{
	if (GameStateManager::GetInstance()->GetCurrentGS() != GS_RESTART)
	{
		GameStateManager::GetInstance()->GSM_Update();
		foLoadGS();
	}
	else
		_nextGS = _currentGS = _previousGS;
}

/* Function to change the game state manually */
void GameStateManager::ChangeNextGameState(unsigned int levelInput)
{
	_nextGS = levelInput;
}

/* Check if escape or windows quit button is clicked */
void GameStateManager::CheckIfQuitState()
{
	//if (AESysDoesWindowExist() == false)
	//{
	//	_nextGS = GS_QUIT;
	//}
}

/* Sets current state to previous state and next state to current state */
void GameStateManager::SetPreviousAndCurrentGS()
{
	_previousGS = _currentGS;
	_currentGS = _nextGS;
}

void GameStateManager::Exit()
{
	if (instance)
		delete instance;
}
