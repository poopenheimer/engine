#include <windows.h>
#include "glew.h"
#include <../glm/glm.hpp>
#include <../glm/gtc/matrix_transform.hpp>

#include "MyGLSL.h"

class GraphicsSystem
{
private:
	static GraphicsSystem * instance;
	HDC windowDC;	// Handle to Windows Device Context
	HGLRC wglDC;	// Handle to GL Rendering Context

	GLuint VAO = { 0 }; // Vertex Array object (store the vertex attribute pointers pointing to VBO)
	GLuint VBO = { 0 }; // vertex buffer object (store large no.of vertices into GPU's memory)
	GLuint EBO = { 0 }; // Element buffer object (tell opengl how we want to draw our object)

	bool InitializeRenderingEnvironment();
	void CleanRenderingEnvironment();

	void GenerateBuffers();
	void BindBuffers();
	void UnbindBuffers();
	void DeleteBuffers();


public:
	static GraphicsSystem * GetInstance()
	{
		if (!instance)
			instance = new GraphicsSystem();
		return instance;
	}

	float vertices[12] = {
		 0.5f,  0.5f, 0.0f,  // top right
		 0.5f, -0.5f, 0.0f,  // bottom right
		-0.5f, -0.5f, 0.0f,  // bottom left
		-0.5f,  0.5f, 0.0f   // top left 
	};
	unsigned int indices[6] = {  // note that we start from 0!
		0, 1, 3,   // first triangle
		1, 2, 3    // second triangle
	};

	GraphicsSystem();
	~GraphicsSystem();

	void Initialize();
	void Update();
	void SwapBuffers(); // function to swap buffers at the end of each frame
	void ResizeWindow();
	void Exit();

	void Draw();

	glm::vec4 bgColor = glm::vec4{0.0f,0.0f,0.0f, 1.0f};

	float xOff = 0;
	float yOff = 0;
};

