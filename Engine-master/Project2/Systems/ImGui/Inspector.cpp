#include "Inspector.h"

#include "../Window/myWindows.h"	//For screen height and width

InspectorWindow::InspectorWindow(std::string name) :
	ImGuiWindowFunctor(name)
{

}

ImGuiWindowFlags InspectorWindow::SetFlags()
{
	ImGuiWindowFlags window_flags = 0;

	static bool no_titlebar = false;
	static bool no_scrollbar = false;
	static bool no_menu = true;
	static bool no_move = true;
	static bool no_resize = false;
	static bool no_collapse = true;
	static bool no_nav = false;
	static bool no_background = false;
	static bool no_bring_to_front = false;

	if (no_titlebar)        window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (no_scrollbar)       window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (!no_menu)           window_flags |= ImGuiWindowFlags_MenuBar;
	if (no_move)            window_flags |= ImGuiWindowFlags_NoMove;
	if (no_resize)          window_flags |= ImGuiWindowFlags_NoResize;
	if (no_collapse)        window_flags |= ImGuiWindowFlags_NoCollapse;
	if (no_nav)             window_flags |= ImGuiWindowFlags_NoNav;
	if (no_background)      window_flags |= ImGuiWindowFlags_NoBackground;
	if (no_bring_to_front)  window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;

	return window_flags;
}

void InspectorWindow::SetPos()
{
	float w = static_cast<float>(MyWindows::GetInstance()->WINDOW_WIDTH);
	ImGui::SetNextWindowPos(ImVec2(w - w/5, 0), ImGuiCond_Always);
}

void InspectorWindow::SetSize()
{
	float w = static_cast<float>(MyWindows::GetInstance()->WINDOW_WIDTH);
	float h = static_cast<float>(MyWindows::GetInstance()->WINDOW_HEIGHT);
	std::cout << "window h = " << MyWindows::GetInstance()->WINDOW_HEIGHT << std::endl;
	ImGui::SetNextWindowSize(ImVec2(w / 5, h), ImGuiCond_Always);
}

void InspectorWindow::MainBody()
{

}
