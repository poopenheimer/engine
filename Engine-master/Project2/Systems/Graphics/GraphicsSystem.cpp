#include "GraphicsSystem.h"
#include "../ImGui/MyImGui.h"
GraphicsSystem* GraphicsSystem::instance = nullptr;

/******************************************************************************/
/*!
  \brief
	Initializes the rendering environment. Steps are as follow:
	1. Assign Device Context(DC) to be used.
	2. Create Pixel Format.
	3. Choose Pixel Format.
	4. Set Pixel Format into DC.
	5. Create OpenGL Context.
	6. Make OpenGL Context Current.
	Returns True if everything has been set properly.
*/
/******************************************************************************/

bool GraphicsSystem::InitializeRenderingEnvironment()
{
	windowDC = GetDC(MyWindows::GetInstance()->m_hWnd);

	//Create Pixel Format
	PIXELFORMATDESCRIPTOR pfdesc = 
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    // Flags
		PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
		32,                   // Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,                   // Number of bits for the depthbuffer
		8,                    // Number of bits for the stencilbuffer
		0,                    // Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	//ChoosePixelFormat takes a device context and PFD struct and returns a pixel format number
	//If it returns 0, then it could not find a pixel format that matches the description, or the PDF was not filled out correctly.
	int pf = ChoosePixelFormat(windowDC, &pfdesc);
	if (pf == 0)
	{
		ReleaseDC(MyWindows::GetInstance()->m_hWnd, windowDC);
		return false;
	}

	//Once we have the pixel format,we set it into the DC with SetPixelFormat
	bool setPixelSuccess = SetPixelFormat(windowDC, pf, &pfdesc);
	if (!setPixelSuccess)
	{
		ReleaseDC(MyWindows::GetInstance()->m_hWnd, windowDC);
		return false;
	}

	// Once we have set pixel format in the DC, create the OpenGL context 
	wglDC = wglCreateContext(windowDC);
	if (!wglDC)
	{
		ReleaseDC(MyWindows::GetInstance()->m_hWnd, windowDC);
		return false;
	}

	// Before we can use OpenGL, context must be made current
	//If there is already a current context, then this function will cause the old context to be replaced with the new.
	bool makeCurrentSuccess = wglMakeCurrent(windowDC, wglDC);
	if (!makeCurrentSuccess)
	{
		wglDeleteContext(wglDC);
		ReleaseDC(MyWindows::GetInstance()->m_hWnd, windowDC);
		return false;
	}

	return true;
}

void GraphicsSystem::CleanRenderingEnvironment()
{
	//Make sure that the context you want to delete is not current
	if (wglDC)
	{
		if (!wglMakeCurrent(NULL, NULL))
		{
			//log
		}
	}

	// After context is not current, you can call wglDeleteContext on it
	if (!wglDeleteContext(wglDC))
	{
		//log
	}
	wglDC = NULL;

	if (windowDC && !ReleaseDC(MyWindows::GetInstance()->m_hWnd, windowDC))
	{
		windowDC = NULL;
	}

	std::cout << "Successfully cleaned rendering environment." << std::endl;
}

void GraphicsSystem::GenerateBuffers()
{
	glGenVertexArrays(1, &VAO);	// Generate Vertex Array Object
	glGenBuffers(1, &VBO);		// Generate Vertex Buffer Object
	glGenBuffers(1, &EBO);		// Generate Element Buffer Object
}

void GraphicsSystem::BindBuffers()
{
	glBindVertexArray(VAO); // Binds my VAO to GL Vertex Array
	glBindBuffer(GL_ARRAY_BUFFER, VBO); // Bind my VBO to GL Array Buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); // binding EBO to buffer
}
void GraphicsSystem::UnbindBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
void GraphicsSystem::DeleteBuffers()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	std::cout << "Successfully deleted buffers" << std::endl;
}

GraphicsSystem::GraphicsSystem()
{

}

GraphicsSystem::~GraphicsSystem()
{

}


void GraphicsSystem::Initialize()
{
	// Create rendering environment
	InitializeRenderingEnvironment();

	GLint res = glewInit();
	if (res != GLEW_OK)
	{
		//Log it
		std::cout << "error GLEW NOT OK" << std::endl;
		assert(true);
	}

	GLenum glErr;
	glErr = glGetError();
	if (glErr != GL_NO_ERROR)
	{
		//Log it
		std::cout << "error GLEW NOT OK" << std::endl;
		assert(true);
	}

	//Ensure that Shader version and OpenGl version are the same.
	std::string versionString = std::string((const char*)glGetString(GL_VERSION));
	std::cout << "OpenGL Version = "<< versionString << std::endl;

	GenerateBuffers();	// Generate VAO, VBO, EBO(If necessary)
	BindBuffers();		// Bind VAO, VBO, EBO(If necessary)

	//Retrieve vertex and fragment shaders from text and compile into shaders accordingly
	MyGLSL::GetInstance()->f_LoadShaderString(
		"Systems/Graphics/Shaders/VertexShader.txt",
		"Systems/Graphics/Shaders/FragmentShader.txt");
	MyGLSL::GetInstance()->Start();

	std::cout << "Initialized: Graphic System" << std::endl;
}

void GraphicsSystem::Update()
{
	// To ensure all items are rendered properly.
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glm::vec4 pt1 = { vertices[0] ,  vertices[1] ,vertices[2]  , 1};
	glm::vec4 pt2 = { vertices[3] ,  vertices[4] ,vertices[5] , 1};
	glm::vec4 pt3 = { vertices[6] ,  vertices[7] ,vertices[8] , 1 };
	glm::vec4 pt4 = { vertices[9] ,  vertices[10] ,vertices[11], 1 };


	static glm::mat4x4 modelmtx;

	glm::translate(modelmtx, glm::vec3{ xOff , yOff , 1.0f });

	pt1 = modelmtx * pt1 + glm::vec4{ xOff , yOff , 0.0f, 1.0f };
	pt2 = modelmtx * pt2 + glm::vec4{ xOff , yOff , 0.0f, 1.0f };
	pt3 = modelmtx * pt3 + glm::vec4{ xOff , yOff , 0.0f, 1.0f };
	pt4 = modelmtx * pt4 + glm::vec4{ xOff , yOff , 0.0f, 1.0f };

	vertices[0] = pt1.x;
	vertices[1] = pt1.y;
	vertices[2] = pt1.z;

	vertices[3] = pt2.x;
	vertices[4] = pt2.y;
	vertices[5] = pt2.z;

	vertices[6] = pt3.x;
	vertices[7] = pt3.y;
	vertices[8] = pt3.z;

	vertices[9] = pt4.x;
	vertices[10] = pt4.y;
	vertices[11] = pt4.z;

	

	Draw();
}

void GraphicsSystem::Draw()
{
	// Allocate memory and stores data in the initialized memory in the currently bound VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	// Allocate memory and stores data in the initialized memory in the currently bound EBO
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	
	//Specify how OpenGL should interpret the Vertex Buffer Data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	// Enable the Vertex Attribute after specifying it.
	glEnableVertexAttribArray(0);

	// Unbind Buffers
	//DO NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
	//UnbindBuffers();

	glUseProgram(MyGLSL::GetInstance()->m_ShaderID);	//Use shader to draw
	glBindVertexArray(VAO);		// Bind the VAO you want to draw
	//glDrawArrays(GL_TRIANGLES, 0, 3);// Draws a triangle using VBO
	
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0); //Use EBO to draw
	glEnableVertexAttribArray(0);
}

void GraphicsSystem::ResizeWindow()
{
	RECT rect;
	GetClientRect(MyWindows::GetInstance()->m_hWnd, &rect);

	glViewport((GLint)0, (GLint)0, rect.right, rect.bottom);
}

void GraphicsSystem::SwapBuffers()
{
	::SwapBuffers(windowDC); //using double buffering
}

void GraphicsSystem::Exit()
{
	DeleteBuffers();				//Clear VAO, VBO, EBO
	MyGLSL::GetInstance()->Clean();	//Clean shader program
	CleanRenderingEnvironment();	//Clear handle to glDC and WindowDC
}

