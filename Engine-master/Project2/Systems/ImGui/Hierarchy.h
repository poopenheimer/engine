#pragma once

#include <string>
#include <list>

#include "ImGuiWindow.h"


class HierarchyWindow : public ImGuiWindowFunctor
{


public:

	std::list<std::string> allGameObjects;	//TODO: Temporarily strings, change to GameObjects.

	HierarchyWindow(std::string name);

	ImGuiWindowFlags SetFlags();
	void SetPos();
	void SetSize();
	void MainBody();

};



